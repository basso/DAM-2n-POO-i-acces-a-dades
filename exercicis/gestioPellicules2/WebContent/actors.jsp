<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="beans.ActorPellicules,java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Llista d'actors</title>
</head>
<body>
	<h1>Llista d'actors</h1>
	<table border='1'>
		<tr>
			<th>Nom</th>
			<th>Cognom</th>
			<th>Pellicula</th>
		</tr>
		<%
			@SuppressWarnings("unchecked")
			List<ActorPellicules> actors = (List<ActorPellicules>) request.getAttribute("actor");
			for (ActorPellicules a : actors) {
		%>
		<tr>
			<td><%=a.getFirstName()%></td>
			<td><%=a.getLastName()%></td>
			<td><%=a.getTitle()%></td>
		</tr>
		<%
			}
		%>
	</table>
	<a href="index.html">Inici</a>
</body>
</html>