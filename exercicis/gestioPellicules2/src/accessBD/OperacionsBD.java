package accessBD;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import beans.ActorPellicules;
import beans.Film;

public class OperacionsBD {
	protected Connection connexio = null;
	protected Statement st = null;
	protected ResultSet rs = null;

	protected Connection getConnection() throws SQLException, ClassNotFoundException {
		if (connexio == null) {
			Class.forName("com.mysql.jdbc.Driver");
			connexio = DriverManager.getConnection("jdbc:mysql://localhost/sakila", "root", "");
		}
		return connexio;
	}

	protected void closeConnection() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				;
			} finally {
				rs = null;
			}
		}
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			} finally {
				st = null;
			}
		}
		if (connexio != null) {
			try {
				connexio.close();
			} catch (SQLException e) {
				;
			} finally {
				connexio = null;
			}
		}
	}

	public List<Film> consultaPellicules() throws SQLException, ClassNotFoundException {
		List<Film> pellicules = new ArrayList<Film>();
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "select * from film;";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				Film f = new Film(rs.getInt("film_id"), rs.getString("title"), rs.getString("description"),
						rs.getString("release_year"), rs.getInt("language_id"), rs.getInt("original_language_id"),
						rs.getInt("length"));
				pellicules.add(f);
			}
		} finally {
			closeConnection();
		}
		return pellicules;
	}

	public List<ActorPellicules> consultaActors() throws SQLException, ClassNotFoundException {
		List<ActorPellicules> actors = new ArrayList<ActorPellicules>();
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "SELECT first_name, last_name, title FROM actor"
					+ " JOIN film_actor ON actor.actor_id = film_actor.actor_id"
					+ " JOIN film ON film_actor.film_id = film.film_id;";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				ActorPellicules a = new ActorPellicules(rs.getString("first_name"), rs.getString("last_name"),
						rs.getString("title"));
				actors.add(a);
			}
		} finally {
			closeConnection();
		}
		return actors;
	}

	public void insereixPellicula(Film f) throws SQLException, ClassNotFoundException {
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "insert into film(film_id, title, description,"
					+ "release_year, language_id, original_language_id," + "length) values('" + f.getFilmId() + "','"
					+ f.getTitle() + "','" + f.getDescription() + "','" + f.getReleaseYear() + "','" + f.getLanguageId()
					+ "','" + f.getOriginalLanguageId() + "','" + f.getLength() + "');";
			st.executeUpdate(sql);
		} finally {
			closeConnection();
		}
	}
}
