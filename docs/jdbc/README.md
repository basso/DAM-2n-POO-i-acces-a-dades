**POO i accés a dades**

M3UF6. POO. Introducció a la persistència en BD
===============================================

- [Introducció](introduccio.md)
- [Instal·lació MySQL](installacio.md)
- [Establiment de connexions](connexio.md)
- [Sentències DML. Select](sentencies_select.md)
- [Sentències preparades](sentencies_preparades.md)
- [Sentències DML i DDL. Modificació de dades](sentencies_modificacio.md)
- [Metadades](metadades.md)
- [Procediments emmagatzemats](procediments.md)
- [Transaccions](transaccions.md)
- [Entorn gràfic i bases de dades](gui.md)
- [Exercicis](exercicis.md)
