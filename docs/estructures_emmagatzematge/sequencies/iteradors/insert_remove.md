##### Inserir i extreure elements a través d'un iterador

A més dels mètodes que hem vist, la interfície *ListIterator* també
incorpora mètodes per afegir i extreure elements a la llista a la
posició on es troba l'iterador, així com per reemplaçar l'element per un
altre.

Quan treballem amb iteradors és important recordar que només podem
modificar la llista a través dels mètodes de l'iterador. L'iterador
emmagatzema la informació necessària sobre l'estat de la llista per tal
de poder retornar els elements anterior i següent de forma consistent.
Si modifiquem la llista sense que l'iterador ho sàpiga, cridant per
exemple el mètode *add* o *remove* de la llista directament, la
informació que té l'iterador ja no serà coherent amb la realitat de la
llista, i ens trobarem amb problemes quan intentem seguir amb el
recorregut.

Com a corol·lari de l'anterior, cal recordar que els iteradors s'han
d'inicialitzar just en el moment d'utilitzar-los, per evitar que hi hagi
modificacions a la llista entre el moment en què s'inicialitza i el
moment en què s'utilitza. No hi ha problema amb declarar la variable al
principi, però la crida a *iterator()* o *listIterator()* s'ha de fer
quan realment volem utilitzar l'iterador.
