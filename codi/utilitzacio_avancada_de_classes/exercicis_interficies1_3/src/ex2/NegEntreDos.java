package ex2;

/**
 * Aquesta classe implementa la sèrie en què cada element
 * es calcula com la meitat de l'anterior canviant el signe.
 *
 */
public class NegEntreDos implements Serie {
	private double actual;

	public NegEntreDos() {
		inicialitza();
	}
	
	public NegEntreDos(double llavor) {
		inicialitza(llavor);
	}
	
	@Override
	public void inicialitza() {
		actual = 1;
	}

	@Override
	public void inicialitza(double llavor) {
		actual = llavor;
	}

	@Override
	public double seguent() {
		actual /= -2;
		return actual;
	}
}
