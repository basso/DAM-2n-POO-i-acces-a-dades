package exemple2;

import java.time.ZonedDateTime;
import java.util.Comparator;

public class VolReal {
	private ZonedDateTime sortida;
	private ZonedDateTime arribada;
	/**
	 * Hem pogut compactar el codi anterior aprofitant que de cada tipus de comparador
	 * només en necessitarem un objecte.
	 * 
	 * Creem els objectes aquí i els guardem com a constants per tal que es puguin utilitzar
	 * des de qualsevol lloc del programa i que no se'n creïn més.
	 * 
	 * Implícitament estem creant una classe (anònima) que implementa Comparator, estem
	 * sobreescrivint el mètode compare, i estem creant un objecte d'aquesta nova classe
	 * que guardem a COMPARADOR_SORTIDES.
	 */
	public static final Comparator<VolReal> COMPARADOR_SORTIDES = new Comparator<VolReal>(){
		@Override
		public int compare(VolReal v1, VolReal v2) {
			return v1.getSortida().compareTo(v2.getSortida());
		}
	};
	public static final Comparator<VolReal> COMPARADOR_ARRIBADES = new Comparator<VolReal>(){
		@Override
		public int compare(VolReal v1, VolReal v2) {
			return v1.getArribada().compareTo(v2.getArribada());
		}
	};
	
	public VolReal(ZonedDateTime sortida, ZonedDateTime arribada) {
		this.sortida = sortida;
		this.arribada = arribada;
	}
	
	public ZonedDateTime getSortida() {
		return sortida;
	}
	
	public ZonedDateTime getArribada() {
		return arribada;
	}
}
